// Concepts and Theory Exam:
// https://forms.gle/GapYaHb6LWkNHw1Z6 


// Mock Technical Exam (Function Coding)
// No need to create a test inputs, the program automatically checks any input through its parameter/s
// Use the parametrs as your test input
// Make sure to use return keyword so your answer will be read by the tester (Example: return result;);
// - Do not add or change parameters
// - Do not create another file
// - Do not share this mock exam

// Use npm install and npm test to test your program

function countLetter(letter, sentence) {
    let result = 0;
    let letterTS= letter.toString();
    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.
    let letterSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    if(!letterSet.includes(letterTS)){
        return undefined;
    }else if(letter.length !== 1){
        return undefined;
    }else{
        for(let i = 0; i < sentence.length; i++) {
            if (sentence[i] === letter){
                result += 1;
            }
        }return result;
    }
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    let textLC = text.toLowerCase();
    let isIsogram = true;
    let letters = textLC.split('');
    letters.forEach((letter, index) =>{
        let lettersAfter = letters.slice(index+1);
        if(lettersAfter.includes(letter)){
            isIsogram = false;
        }
    })
    return isIsogram;
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    if(age < 13){
        console.log(undefined);
        return;
    }else if(age >= 13 && age <= 21){
        console.log((Math.round((price*0.8)*100)/100).toString());
        return (Math.round((price*0.8)*100)/100).toString();
    }else if (age >= 22 && age <= 64){
        console.log(Math.round(price).toString())
        return (Math.round((price)*100)/100).toString();
    }else if (age >= 65){
        console.log(Math.round(price*0.8).toString())
        return (Math.round((price*0.8)*100)/100).toString();
    }else{
        console.log(undefined);
        return;
    }
    
    
}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
    let whatsHot = [];
    items.forEach(item => {
        let category = item.category;
        if(!whatsHot.includes(category) && item.stocks === 0){
            whatsHot.push(category);
        }
    });
    return whatsHot;

}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    let flyingVoters = [];
    for (let i= 0; i< candidateA.length; i++){
        if(candidateB.includes(candidateA[i])){
            flyingVoters.push(candidateA[i]);
        }
    }
    return flyingVoters;
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};